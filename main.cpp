#include <QCoreApplication>
#include <QThread>
#include <iostream>

#include "Device/Includes.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << "Main thread" << QThread::currentThread();

    QThread ovenThread;

    ovenThread.start();

    EthernetDevice *dev = new EthernetDevice();

    dev->moveToThread(&ovenThread);

    ModuleInfo& info = dev->GetDeviceInfo();

    //    dev->set_NeedCRC(true); // OWEN

    //    dev->set_2byteState(true); // OWEN

    //    info.DeviceAddress = 16; //OWEN

//    dev->set_NeedCRC(false); // TM-PD3R3

//    dev->set_2byteState(false); // TM-PD3R3

    info.DeviceAddress = 1; //TM-PD3R3

    info.InputCount = 6;

        info.InputRegister = 0x33;    // Не нужно для DCON протокола

        info.InputRegisterCount = 1;

        info.OutputCount = 8;

        info.OutputRegister = 0x1D6;

        info.OutputRegisterCount = 1;

    info.CalculateDeviceAddress();

    dev->SetDeviceInfo(info);

    //    dev->SetModBusCheckFuntction(ModBusCheckFunctions::ModBus_4);

    int VID{ 0x1a86 };

    int PID{ 0x7523 };

    int BaudRate{ 115200 };

    dev->Connect("10.10.69.101", 502);

    dev->Start();

    std::string oper;

    std::cout << "Write operation" << std::endl;

    std::cin >> oper;

    while (oper != "001")
    {
        if (oper == "w10")
        {
            dev->DeactivateDO(0);
        }
        if (oper == "w11")
        {
            dev->ActivateDO(0);
        }
        if (oper == "w20")
        {
            dev->DeactivateDO(1);
        }
        if (oper == "w21")
        {
            dev->ActivateDO(1);
        }
        if (oper == "w30")
        {
            dev->DeactivateDO(2);
        }
        if (oper == "w31")
        {
            dev->ActivateDO(2);
        }
        if (oper == "w40")
        {
            dev->DeactivateDO(3);
        }
        if (oper == "w41")
        {
            dev->ActivateDO(3);
        }
        if (oper == "tra")
        {
            dev->ToggleDO(0, 1000, 1000);
            dev->ToggleDO(1, 800, 800);
            dev->ToggleDO(2, 600, 600);
            dev->ToggleDO(3, 400, 400);
        }

        std::cout << "Write operation" << std::endl;
        std::cin >> oper;
    }


    return a.exec();
}
