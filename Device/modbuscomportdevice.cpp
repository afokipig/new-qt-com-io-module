#include "modbuscomportdevice.h"

#include <QDebug>
#include <iostream>
#include <stdexcept>

#include "modbuswriter.h"

namespace ModbusRTU_Commands
{

int ModBusRTU_1_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    int comLenght = ModBusWriter::ModBus_1_Command(0, deviceAddress, inputRegister, registersCount, out_command, lenght);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;
}

int ModBusRTU_2_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    int comLenght = ModBusWriter::ModBus_2_Command(0, deviceAddress, inputRegister, registersCount, out_command, lenght);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;
}

int ModBusRTU_3_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    int comLenght = ModBusWriter::ModBus_3_Command(0, deviceAddress, inputRegister, registersCount, out_command, lenght);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;
}

int ModBusRTU_4_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    int comLenght = ModBusWriter::ModBus_4_Command(0, deviceAddress, inputRegister, registersCount, out_command, lenght);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;

}

int ModBusRTU_5_Command(const int deviceAddress, const int outputRegister, const int outValue, char, char *out_command, const int lenght, char)
{
    int comLenght = ModBusWriter::ModBus_5_Command(0, deviceAddress, outputRegister, outValue, out_command, lenght);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;

}

int ModBusRTU_6_Command(const int deviceAddress, const int outputRegister, const int outValue, char, char *out_command, const int lenght, char)
{
    int comLenght = ModBusWriter::ModBus_6_Command(0, deviceAddress, outputRegister, outValue, out_command, lenght);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;

}

int ModBusRTU_15_Command(const int deviceAddress, const int outputRegister, const int registersCount, char byteMaskLO, char *out_command, const int lenght, char byteMaskHi)
{
    int comLenght = ModBusWriter::ModBus_15_Command(0, deviceAddress, outputRegister, registersCount, byteMaskLO, out_command, lenght, byteMaskHi);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;

}

int ModBusRTU_16_Command(const int deviceAddress, const int outputRegister, const int registersCount, char byteMaskLO, char *out_command, const int lenght, char byteMaskHi)
{
    int comLenght = ModBusWriter::ModBus_16_Command(0, deviceAddress, outputRegister, registersCount, byteMaskLO, out_command, lenght, byteMaskHi);

    uint16_t crc = ModBusWriter::CRC16(out_command, ++comLenght);

    out_command[comLenght] = (crc & 0xff);

    out_command[++comLenght] = (crc >> 8);

    return comLenght;

}
//*******************************************************************************************************************************************

}





ModbusComPortDevice::ModbusComPortDevice(QObject *parent)
    : ComPortDevice{ parent }
    , ModbusBasic{ ModbusRTU_Commands::ModBusRTU_3_Command,
                   ModbusRTU_Commands::ModBusRTU_16_Command }
{ }
//*******************************************************************************************************************************************



void ModbusComPortDevice::SetModBusCheckFuntction(const ModBusCheckFunctions functionNum)
{

    switch (functionNum)
    {
    case ModBusCheckFunctions::ModBus_1:

        CheckDigitalInput = ModbusRTU_Commands::ModBusRTU_1_Command;

        break;

    case ModBusCheckFunctions::ModBus_2:

        CheckDigitalInput = ModbusRTU_Commands::ModBusRTU_2_Command;

        break;

    case ModBusCheckFunctions::ModBus_3:

        CheckDigitalInput = ModbusRTU_Commands::ModBusRTU_3_Command;

        break;

    case ModBusCheckFunctions::ModBus_4:

        CheckDigitalInput = ModbusRTU_Commands::ModBusRTU_4_Command;

        break;

    }

}
//*******************************************************************************************************************************************



void ModbusComPortDevice::SetModBusSetFunction(const ModBusSetFunctions functionNum)
{

    switch (functionNum)
    {
    case ModBusSetFunctions::ModBus_5:

        SetDigitalOutput = ModbusRTU_Commands::ModBusRTU_5_Command;

        break;

    case ModBusSetFunctions::ModBus_6:

        SetDigitalOutput = ModbusRTU_Commands::ModBusRTU_6_Command;

        break;

    case ModBusSetFunctions::ModBus_15:

        SetDigitalOutput = ModbusRTU_Commands::ModBusRTU_15_Command;

        break;

    case ModBusSetFunctions::ModBus_16:

        SetDigitalOutput = ModbusRTU_Commands::ModBusRTU_16_Command;

        break;

    }

}
//*******************************************************************************************************************************************



void ModbusComPortDevice::SetRelayState(unsigned long state)
{

    if(!SetDigitalOutput)
    {

        qDebug() << this << "Set function is NULL";

        return;
    }

    NextCommandSize = SetDigitalOutput(DeviceInfo.DeviceAddress, DeviceInfo.OutputRegister, DeviceInfo.OutputRegisterCount, state, NextCommand, MAX_COMMAND_SIZE, 0);

    std::cout << "Write comand" << std::endl;

    for(int i = 0; i < NextCommandSize + 1; ++i)
    {
        std::cout << std::hex << (unsigned int)NextCommand[i] << ' ';
    }

    std::cout << std::endl;

    Write(NextCommand, NextCommandSize + 1);

}
//*******************************************************************************************************************************************



void ModbusComPortDevice::CheckDI()
{

    if(!CheckDigitalInput)
    {

        qDebug() << this << "Check DI function is NULL";

        return;
    }

    NextCommandSize = CheckDigitalInput(DeviceInfo.DeviceAddress, DeviceInfo.InputRegister, DeviceInfo.InputRegisterCount, NextCommand, MAX_COMMAND_SIZE);

    bReadNextResponse = true;

    Write(NextCommand, NextCommandSize + 1);

}
//*******************************************************************************************************************************************



void ModbusComPortDevice::HandleModuleResponse(const QByteArray &response)
{
    if((unsigned int)response.at(1) > 0x80 && (unsigned int)response.at(1) < 256)
    {
        try
        {
            qDebug() << this << "Write error" << WriteErrors.at(response.at(2));

            emit SendInfoMessage( WriteErrors.at(response.at(2)) );

            return;
        }
        catch (std::out_of_range& e)
        {

            qDebug() << this << "Error not find" << e.what();

            emit SendInfoMessage( QString::number(response.at(2)) + ' ' + QString(e.what()) );

            return;
        }
    }

    if(!(response.at(1) == 0x03 || response.at(1) == 0x04))
    {
        std::cout << "Int Val" << response.data() << std::endl;

        for(int i = 0; i < response.length(); ++i)
        {
            std::cout << (int)response.at(i) << ' ';
        }
        std::cout << std::endl << "Hex Val" << std::endl;

        for(int i = 0; i < response.length(); ++i)
        {
            std::cout << std::hex << (int)response.at(i) << ' ';
        }
        std::cout << std::endl;

        return;
    }

    for(int i = 0; i < response.at(2); i += 2)
    {
        InputReleState = (InputReleState << 8) + response.at(i + 3);

        InputReleState = (InputReleState << 8) + response.at(i + 4);
    }

    if(!TriggeregState())
    {
        InputReleState = ~InputReleState;
    }

    emit SensorState(InputReleState);
}
//*******************************************************************************************************************************************



void ModbusComPortDevice::SpecialRead()
{

    if(!SimpleUseDevice->isReadable())
    {
        return;
    }

    QByteArray response{SimpleUseDevice->readAll()};

    if((unsigned int)response.at(1) > 0x80 && (unsigned int)response.at(1) < 256)
    {
        try
        {
            qDebug() << this << "Write error" << WriteErrors.at(response.at(2));

            emit SendInfoMessage( WriteErrors.at(response.at(2)) );

            return;
        }
        catch (std::out_of_range& e)
        {

            qDebug() << this << "Error not find" << e.what();

            emit SendInfoMessage( QString::number(response.at(2)) + ' ' + QString(e.what()) );

            return;
        }
    }

    for(int i = 0; i < response.at(2); i += 2)
    {
        OutputState = (InputReleState << 8) + response.at(i + 3);

        OutputState = (InputReleState << 8) + response.at(i + 4);
    }

}
//*******************************************************************************************************************************************
