#include "comportdevice.h"

#include <QDebug>

#include <QSerialPortInfo>



ComPortDevice::ComPortDevice(QObject *parent)
    : IO_Device(parent) { }
//*******************************************************************************************************************************************



const QString ComPortDevice::FindDevice(const qint16 VID, const qint16 PID)
{

    const auto portsInfo = QSerialPortInfo::availablePorts();

    for(const auto &port : portsInfo)
    {

        if((port.vendorIdentifier() == VID) && (port.productIdentifier() == PID))
        {

            qDebug() << port.vendorIdentifier() << port.productIdentifier();

            return port.portName();

        }

    }

    qDebug() << "Device not found";

    return "NotFound: VID - " + QString::fromStdString(IntToHexStdString(VID)).toUpper() + ", PID - " + QString::fromStdString(IntToHexStdString(PID)).toUpper();

}
//*******************************************************************************************************************************************



const bool ComPortDevice::ConnectCOM()
{

    if(PortNum == "NotFound") return false;

    qInfo() << this << "ConnectCOM" << PortNum << " - is port Num " << PortBaudRate << " - Is baud rate";

    try
    {

        SimpleUseDevice->setPortName(PortNum);


    }  catch (std::exception &ex)
    {

        qDebug() << this << "ConnectCOM throw exception" << QString(ex.what());

    }  catch (...)
    {

        qDebug() << this << "ConnectCOM throw unexpected exception";

    }

    //qDebug() << this << "Set port name success";

    if(SimpleUseDevice->open(QIODevice::ReadWrite)/* && baudRate != 0*/)
    {

        qDebug() << this << "Port open";

        SimpleUseDevice->setBaudRate(PortBaudRate);

        qDebug() << this << "Port setBaudRate success";

        SimpleUseDevice->setDataBits(QSerialPort::Data8);

        qDebug() << this << "Port SetDataBits success";

        SimpleUseDevice->setParity(QSerialPort::NoParity);

        qDebug() << this << "Port setParity success";

        SimpleUseDevice->setStopBits(QSerialPort::OneStop);

        qDebug() << this << "Port setStopBits success";

        SimpleUseDevice->setFlowControl(QSerialPort::NoFlowControl);

        qDebug() << this << "Port setFlowControl success";

        SimpleUseDevice->setDataTerminalReady(false);

        qDebug() << this << "Port SetDataTerminalReady success";

        SimpleUseDevice->setRequestToSend(false);

        qDebug() << this << "Port setRequestToSend success";

        qDebug() << this << "Port name" << PortNum;

        ErrorList.clear();

        return true;

    }

    return false;

}
//*******************************************************************************************************************************************



void ComPortDevice::HandlePortError(const QSerialPort::SerialPortError &error)
{
    switch (error)
    {
    case QSerialPort::NoError:

        qDebug() << this << "Error: NoError";

        break;

    case QSerialPort::DeviceNotFoundError:

        qDebug() << this << "Error: DeviceNotFoundError";

        Reconnect();

        break;

    case QSerialPort::PermissionError:

        qDebug() << this << "Error: PermissionError";

        Reconnect();

        break;

    case QSerialPort::OpenError:

        qDebug() << this << "Error: OpenError";

        Reconnect();

        break;

    case QSerialPort::WriteError:

        qDebug() << this << "Error: WriteError";

        SetReadyWrite();

        break;

    case QSerialPort::ReadError:

        qDebug() << this << "Error: ReadError";

        SetReadyWrite();

        break;

    case QSerialPort::ResourceError:

        qDebug() << this << "Error: ResourceError";

        Reconnect();

        break;

    case QSerialPort::UnsupportedOperationError:

        break;

    case QSerialPort::UnknownError:

        qDebug() << this << "Error: UnknownError";

        Reconnect();

        break;

    case QSerialPort::TimeoutError:

        qDebug() << this << "Error: TimeoutError";

        Reconnect();

        break;
    case QSerialPort::NotOpenError:

        qDebug() << this << "Error: NotOpenError";

        Reconnect();

        break;

    default:

        qDebug() << this << "Error: Deprecated error";

        Reconnect();

        break;

    }

}
//*******************************************************************************************************************************************



bool ComPortDevice::Reconnect(bool bForced)
{
    Q_UNUSED(bForced);

    badRequest = 0;

    SetIsConnected(false);

    if(SimpleUseDevice->isOpen())
    {

        SimpleUseDevice->close();

    }

    bool bIsSuccess = ConnectCOM();

    bIsStarted = false;

    Start();

    return bIsSuccess;

}
//*******************************************************************************************************************************************



const bool ComPortDevice::InitPortAdapter(const int VID, const int PID, const int baundRate)
{

    Identifires = std::make_pair(VID, PID);

    PortNum = FindDevice(VID, PID);

    emit SendInfoMessage(PortNum);

    PortBaudRate = baundRate;

    if(PortNum.contains("NotFound"))
    {
        return false;
    }

    return QMetaObject::invokeMethod(this, "InitPortAdapterPrivate", Qt::BlockingQueuedConnection);

}
//*******************************************************************************************************************************************



const bool ComPortDevice::InitPortAdapterPrivate()
{

    if(!Device)
    {

        SimpleUseDevice = new QSerialPort();

        SimpleUseDevice->moveToThread(thread());

        Device = SimpleUseDevice;

    }
    else if(SimpleUseDevice->isOpen())
    {
        SimpleUseDevice->close();
    }

    bool bIsInit{ ConnectCOM() };

    SetIsConnected(bIsInit);

    emit SendSensorInitResult(PortNum, bIsInit);

    if(bIsInit)
    {

        connect(SimpleUseDevice, &QSerialPort::readyRead, this, &ComPortDevice::Read, Qt::QueuedConnection);

        connect(SimpleUseDevice, &QSerialPort::errorOccurred, this, &ComPortDevice::GetPortError, Qt::QueuedConnection);

    }

    return bIsInit;

}
//*******************************************************************************************************************************************



void ComPortDevice::GetPortError(const QSerialPort::SerialPortError error)
{

    if(error == QSerialPort::NoError)
    {

        return;

    }

    if(!ErrorList.contains(error))
    {

        ErrorList.push_back(error);

    }

    if(ErrorList.empty())
    {

        return;

    }

    QSerialPort::SerialPortError errorToHandle = ErrorList.first();

    ErrorList.pop_front();

    while(!ErrorList.empty())
    {

        HandlePortError(errorToHandle);

    }

}
//*******************************************************************************************************************************************
