#pragma once

#include "comportdevice.h"
#include "modbusbasic.h"



class ModbusComPortDevice : public ComPortDevice, public ModbusBasic
{

    // Q_OBJECT Macro

    Q_OBJECT

    //*******************************************************************************************************************************************



    // Constructors and destructor
public:

    explicit ModbusComPortDevice(QObject *parent = nullptr);

    //*******************************************************************************************************************************************



    // Public methods
public:

    void SetModBusCheckFuntction(const ModBusCheckFunctions functionNum) override;

    void SetModBusSetFunction(const ModBusSetFunctions functionNum) override;

    //*******************************************************************************************************************************************



    // Protected methods
protected:

    virtual void SetRelayState(unsigned long state) override;

    virtual void CheckDI() override;

    virtual void HandleModuleResponse(const QByteArray &response) override;


    //*******************************************************************************************************************************************



    // Private methods
private:

    //*******************************************************************************************************************************************



    // Signals
signals:

    //*******************************************************************************************************************************************



    // Public slots
public slots:

    //*******************************************************************************************************************************************



    // Protected slots
protected slots:

    virtual void SpecialRead() override;

    //*******************************************************************************************************************************************



    // Private slots
private slots:

    //*******************************************************************************************************************************************



    // Public variables
public:

    //*******************************************************************************************************************************************



    // Protected variables
protected:

    //*******************************************************************************************************************************************



    // Private variables
private:

    std::map<int, QString> WriteErrors
    {
        {0x01,"Function not used"},
        {0x02,"Address invalid"},
        {0x03,"Value invalid"},
        {0x04,"Slave Device Failure"},
        {0x05,"Acknowledge"},
        {0x06,"Slave Device Busy"},
        {0x07,"Negative Acknowledge"},
        {0x08,"Memory Parity Error"},
        {0x0A,"Gateway Path Unavailable"},
        {0x0B,"Gateway Target Device Failed to Respond"},

    };

};
/*

    //qDebug() << this << "Read" << response << (int)response.at(1) << response.at(1);

    if((int)response.at(1) + 256 > 0x80 && (int)response.at(1) + 256 < 256)
    {

        switch ((int)response.at(2))
        {
        case 0x01:

            qWarning() << this << "Modbus error" << "Function not used";

            break;

        case 0x02:

            qWarning() << this << "Modbus error" << "Address invalid";

            break;

        case 0x03:

            qWarning() << this << "Modbus error" << "Value invalid";

            break;

        case 0x04:

            qWarning() << this << "Modbus error" << "4";

            break;

        case 0x05:

            qWarning() << this << "Modbus error" << "5";

            break;

        case 0x06:

            qWarning() << this << "Modbus error" << "6";

            break;

        case 0x07:

            qWarning() << this << "Modbus error" << "7";

            break;

        case 0x08:

            qWarning() << this << "Modbus error" << "8";

            break;

        default:

            qWarning() << this << "Modbus error" << "unexpected";

            break;
        }

    }


*/
