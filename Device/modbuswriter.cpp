#include "modbuswriter.h"
#include <QtGlobal>

int ModBusWriter::ModBus_1_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x01;

    out_command[++startPos] = (inputRegister >> 8);
    out_command[++startPos] = (inputRegister & 0xFF);

    out_command[++startPos] = (registersCount >> 8);
    out_command[++startPos] = (registersCount & 0xFF);

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBus_2_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x02;

    out_command[++startPos] = (inputRegister >> 8);
    out_command[++startPos] = (inputRegister & 0xFF);

    out_command[++startPos] = (registersCount >> 8);
    out_command[++startPos] = (registersCount & 0xFF);

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBus_3_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x03;

    out_command[++startPos] = (inputRegister >> 8);
    out_command[++startPos] = (inputRegister & 0xFF);

    out_command[++startPos] = (registersCount >> 8);
    out_command[++startPos] = (registersCount & 0xFF);

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBus_4_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x04;

    out_command[++startPos] = (inputRegister >> 8);
    out_command[++startPos] = (inputRegister & 0xFF);

    out_command[++startPos] = (registersCount >> 8);
    out_command[++startPos] = (registersCount & 0xFF);

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBus_5_Command(int startPos, const int deviceAddress, const int outputRegister, const bool bIsActive, char *out_command, const int lenght)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x05;

    out_command[++startPos] = (outputRegister >> 8);
    out_command[++startPos] = (outputRegister & 0xFF);

    out_command[++startPos] = bIsActive ? 0xFF : 0x00;
    out_command[++startPos] = 0x00;

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBus_6_Command(int startPos, const int deviceAddress, const int outputRegister, const int outValue, char *out_command, const int lenght)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x06;

    out_command[++startPos] = (outputRegister >> 8);
    out_command[++startPos] = (outputRegister & 0xFF);

    out_command[++startPos] = (outValue >> 8);
    out_command[++startPos] = (outValue & 0xFF);

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBus_15_Command(int startPos, const int deviceAddress, const int outputRegister, const int registersCount, const char byteMaskLO, char *out_command, const int lenght, char byteMaskHi)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x0F;

    out_command[++startPos] = (outputRegister >> 8);
    out_command[++startPos] = (outputRegister & 0xFF);

    out_command[++startPos] = (registersCount >> 8);
    out_command[++startPos] = (registersCount & 0xFF);


    if(byteMaskHi == -1)
    {

        out_command[++startPos] = 0x01;

        out_command[++startPos] = byteMaskLO;

    }
    else
    {

        out_command[++startPos] = 0x02; // Количество байт далее

        out_command[++startPos] = byteMaskLO; // Значение байт (байтовая маска) 1 - 8

        out_command[++startPos] = byteMaskHi; // Значение байт (байтовая маска) 9 - 16

    }

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBus_16_Command(int startPos, const int deviceAddress, const int outputRegister, const int registersCount, const char byteMaskLO, char *out_command, const int lenght, char byteMaskHi)
{
    Q_UNUSED(lenght);

    out_command[startPos] = deviceAddress;

    out_command[++startPos] = 0x10;

    out_command[++startPos] = (outputRegister >> 8);
    out_command[++startPos] = (outputRegister & 0xFF);

    out_command[++startPos] = (registersCount >> 8);
    out_command[++startPos] = (registersCount & 0xFF);


    if(byteMaskHi == -1)
    {

        out_command[++startPos] = 0x01;

        out_command[++startPos] = byteMaskLO;

    }
    else
    {

        out_command[++startPos] = 0x02; // Количество байт далее

        out_command[++startPos] = byteMaskHi; // Значение байт (байтовая маска) 1 - 8

        out_command[++startPos] = byteMaskLO; // Значение байт (байтовая маска) 9 - 16

    }

    return startPos;

}/*******************************************************************************************************************************************/



int ModBusWriter::ModBusTCP_Header(const int transactrionNumber, int startPos, char* out_command)
{

    out_command[startPos] = 0x00;
    out_command[++startPos] = transactrionNumber;

    out_command[++startPos] = 0x00;
    out_command[++startPos] = 0x00;

    out_command[++startPos] = 0x00;
    out_command[++startPos] = 0x00;

    return startPos;

}/*******************************************************************************************************************************************/



const unsigned short ModBusWriter::CRC16(const char *data, const int size)
{

    unsigned short crc = 0xFFFF;

    for(int i = 0; i < size; ++i)
    {
        crc ^= (unsigned short)data[i];

        for(int j = 8; j != 0; --j)
        {
            if((crc & 0x0001) !=0)
            {
                crc >>= 1;

                crc ^= 0xA001;
            }
            else
            {
                crc >>= 1;
            }
        }
    }

    return crc;

}/*******************************************************************************************************************************************/



