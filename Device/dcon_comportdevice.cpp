#include "dcon_comportdevice.h"
#include <QDebug>

DCON_ComPortDevice::DCON_ComPortDevice(QObject *parent) : ComPortDevice(parent)
{
    
}

void DCON_ComPortDevice::SetRelayState(unsigned long state)
{

    int idx{ 0 };

    NextCommand[idx] = '@';

    std::string strDeviceAddress{ IntToHexStdString( (int)static_cast<unsigned int>(DeviceInfo.DeviceAddress)) };

    NextCommand[++idx] = DeviceInfo.DeviceAddress_str.at(0);
    NextCommand[++idx] = DeviceInfo.DeviceAddress_str.at(1);

    std::string nextState{ IntToHexStdString(state) };

    if(b2byteState)
    {
        NextCommand[++idx] = nextState.at(nextState.size()-2);
    }

    NextCommand[++idx] = nextState.at(nextState.size()-1);

    if(bNeedCRC)
    {
        std::string checkSum{ IntToHexStdString(CalculateCheckSum(NextCommand, 5)) };

        NextCommand[++idx] = checkSum.at(checkSum.size()-2);
        NextCommand[++idx] = checkSum.at(checkSum.size()-1);
    }

        NextCommand[++idx] = 0x0D;

        Write(NextCommand, ++idx);

//    std::cout << "command " << idx << '\n';
//    for (qint64 i = 0; i < idx; ++i)
//    {
//        std::cout << std::hex << (int)NextCommand[i] << ' ';
//    }
//    std::cout << "\ncommand\n";
}

void DCON_ComPortDevice::CheckDI()
{

    bReadNextResponse = true;

    int idx{ 0 };

    NextCommand[idx] = '@';

    NextCommand[++idx] = DeviceInfo.DeviceAddress_str.at(0);
    NextCommand[++idx] = DeviceInfo.DeviceAddress_str.at(1);

    if(bNeedCRC)
    {
        std::string checkSum{ IntToHexStdString(CalculateCheckSum(NextCommand, 3)) };

        NextCommand[++idx] = checkSum[0];
        NextCommand[++idx] = checkSum[1];
    }

    NextCommand[++idx] = 0x0D;

    Write(NextCommand, ++idx);

}

void DCON_ComPortDevice::HandleModuleResponse(const QByteArray &response)
{

    if(!bReadNextResponse || response.size() < 5) return;

    char tmp[4] = {'0', 'x'};

    tmp[2] = response.at(3);

    tmp[3] = response.at(4);

    InputReleState = (unsigned long)strtol(tmp, nullptr, 16);

    emit SensorState(InputReleState);

}

void DCON_ComPortDevice::SpecialRead()
{

    if(!Device->isReadable()) return;

    QByteArray response{ Device->readAll() };

    if(!bReadNextResponse || response.size() < 5) return;

    char tmp[4] = {'0', 'x'};

    tmp[2] = response.at(3);

    tmp[3] = response.at(4);

    OutputState = (unsigned long)strtol(tmp, nullptr, 16);

}

void DCON_ComPortDevice::set_2byteState(bool newB2byteState)
{
    b2byteState = newB2byteState;
}

bool DCON_ComPortDevice::NeedCRC() const
{
    return bNeedCRC;
}

void DCON_ComPortDevice::set_NeedCRC(bool newBNeedCRC)
{
    bNeedCRC = newBNeedCRC;
}

const int DCON_ComPortDevice::CalculateCheckSum(char* command, int lenght)
{

    int c = 0;

    for(int i = 0; i < lenght; ++i)
    {
        c += command[i];
    }

    return c;

}
