#pragma once


namespace ModBusWriter
{

     int ModBus_1_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char* out_command, const int lenght);

     int ModBus_2_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char* out_command, const int lenght);

     int ModBus_3_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char* out_command, const int lenght);

     int ModBus_4_Command(int startPos, const int deviceAddress, const int inputRegister, const int registersCount, char* out_command, const int lenght);

     int ModBus_5_Command(int startPos, const int deviceAddress, const int outputRegister, const bool bIsActive, char *out_command, const int lenght);

     int ModBus_6_Command(int startPos, const int deviceAddress, const int outputRegister, const int outValue, char *out_command, const int lenght);

     int ModBus_15_Command(int startPos, const int deviceAddress, const int outputRegister, const int registersCount, const char byteMaskLO, char *out_command, const int lenght, char byteMaskHi = -1);

     int ModBus_16_Command(int startPos, const int deviceAddress, const int outputRegister, const int registersCount, const char byteMaskLO, char *out_command, const int lenght, char byteMaskHi = -1);

     int ModBusTCP_Header(const int transactrionNumber, int startPos, char* out_command);

    /**
     * @brief Calculate checksum of command and push it in command (Used in ModBus commands)
     */
    const unsigned short CRC16(const char* data, const int size);

};

