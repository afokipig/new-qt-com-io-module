#pragma once

#include <QObject>
#include <QMutex>
#include <QThread>
#include <QTimer>
#include <QIODevice>

#include <iostream>

#define MAX_COMMAND_SIZE 30


struct ModuleInfo
{

    int DeviceAddress;

    std::string DeviceAddress_str;

    int InputRegisterCount;

    int OutputRegisterCount;

    int InputCount;

    int OutputCount;

    int InputRegister;

    int OutputRegister;

    void CalculateDeviceAddress();

};


class IO_Device : public QObject
{

    // Q_OBJECT Macro

    Q_OBJECT

    //*******************************************************************************************************************************************



    // Private structs

    enum class TypeOfOperaton
    {
        EnableOut,
        DisableOut
    };

    struct Operation
    {

        TypeOfOperaton OperationType;

        int FirstArg;

        int SecondArg;

        Operation(const TypeOfOperaton &newOperationtype, const int &newFirsArg, const int &newSecondArg = -1)
            : OperationType(newOperationtype),
              FirstArg(newFirsArg),
              SecondArg(newSecondArg) { };

    };

    //*******************************************************************************************************************************************



    // Constructors and destructor
public:

    explicit IO_Device(QObject *parent = nullptr);

    virtual ~IO_Device();

    //*******************************************************************************************************************************************



    // Public methods
public:

    // Getters and Setters

    ModuleInfo &GetDeviceInfo();

    void SetDeviceInfo(ModuleInfo& newDeviceInfo);

    bool IsStarted() const;

    void SetIsStarted(bool newBIsStarted);

    bool b_WriteModeEnable() const;

    void Set_b_WriteModeEnable(bool newBWriteModeEnable);

    bool b_ReadModeEnable() const;

    void Set_b_ReadModeEnable(bool newBReadModeEnable);

    bool IsWorked() const;

    void SetIsWorked(bool newIsWorked);

    bool b_ReadNextResponse() const;

    void Set_b_ReadNextResponse(bool newBReadNextResponse);

    bool b_SaveOutputStates() const;

    void Set_b_SaveOutputStates(bool newBSaveOutputStates);

    const std::string &str_DeviceAddress() const;

    char cstr_DeviceAddress() const;

    bool ReadyWrite() const;

    bool TriggeregState() const;

    void setTriggeregState(bool newTriggeregState);

    bool IsConnected() const;

    void setIsConnected(bool newIsConnected);

    /**
     * @brief Convert dec number to hex string
     */
    static const std::string IntToHexStdString(const int &number);

    //*******************************************************************************************************************************************



    // Protected methods
protected:

    virtual void Write(const QByteArray& message);

    virtual void Write(const char* message, const qint64 lenght);

    /**
     * @brief Send command to change state of relay output by number
     */
    virtual void ChangeRelayOutput(const int outputNumber, bool bIsActive);

    /**
     * @brief Send command to read states of relay input
     */
    virtual void CheckDI() = 0;

    /**
     * @brief Handle module response to last command
     */
    virtual void HandleModuleResponse(const QByteArray& response) = 0;

    /**
     * @brief Return port state
     */
    bool b_ReadyWrite() const;

    /**
     * @brief Set port state as ready to write
     */
    void Set_b_ReadyWrite(bool newBReadyWrite);

    /**
     * @brief Check the port ready to execute next command
     */
    void CheckReadiness();

    /**
     * @brief Set bit at number{ 0 }
     */
    static char OffBit(char byte, const int number);

    /**
     * @brief Set bit at number = 1
     */
    static char OnBit(char byte, const int number);

    // Connect methods

    /**
     * @brief Set new connected state. If state changed - emit ConnectingStatusChanged(bool newStatus) signal
     */
    void SetIsConnected(bool newIsConnected);

    /**
     * @brief Reconnect to IODevice. If bForced - forced close active connections and open new
     */
    virtual bool Reconnect(bool bForced = false) = 0;

    /**
     * @brief Set port read slot to special
     */
    void ReconnectToReadSpecialResponse();

    /**
     * @brief Set port read slot to standart
     */
    void ConnectIODevice();

    /**
     * @brief Set byte mask to relay output
     */
    virtual void SetRelayState(const unsigned long state) = 0;

    //*******************************************************************************************************************************************



    // Private methods
private:

    void AddOperation(const Operation& operation);

    const Operation& GetOperation();

    //*******************************************************************************************************************************************



    // Signals
signals:

    /**
     * @brief Send state of digital input by number
     */
    void SensorState(const long byteMask);

    /**
     * @brief Send result of device initialization
     */
    void SendSensorInitResult(const QString& comName, const bool bIsInit);

    /**
     * @brief Send result of device initialization
     */
    void ConnectingStatusChanged(const bool newStatus);

    /**
     * @brief Send information message
     */
    void SendInfoMessage(const QString& message);

    //*******************************************************************************************************************************************



    // Public slots
public slots:

    /**
     * @brief Start a port cicle
     */
    void Start();

    /**
     * @brief Stop a port cicle
     */
    void Stop();

    // Functions slots

    /**
     * @brief Activate digital output by number
     */
    void ActivateDO(const int outputNumber);

    /**
     * @brief Deactivate digital output by number
     */
    void DeactivateDO(const int outputNumber);

    /**
     * @brief Activate digital output by number after sheddule time, and deactivate them after delay time
     */
    void ToggleDO(const int outputNumber, const int shedulleTime = 100, const int delayTime = 50);

    //*******************************************************************************************************************************************



    // Protected slots
protected slots:

    /**
     * @brief Set object state as ready to write
     */
    void SetReadyWrite();

    /**
     * @brief Handle device timeout
     */
    void HandleTimeout();

    /**
     * @brief Read device response
     */
    void Read();

    /**
     * @brief Read device response for specific usage
     */
    virtual void SpecialRead() = 0;

    //*******************************************************************************************************************************************



    // Private slots
private slots:

    //*******************************************************************************************************************************************



    // Public variables
public:

    //*******************************************************************************************************************************************



    // Protected variables
protected:

    QTimer AlertTimer;

    QIODevice* Device = nullptr;

    ModuleInfo DeviceInfo;

    int NextCommandSize;

    int badRequest{ 0 };

    char NextCommand[MAX_COMMAND_SIZE];

    unsigned long OutputState{ 0 };

    unsigned long NewOutputState{ 0 };

    unsigned long InputReleState{ 0 };

    bool bIsStarted{ false };

    bool bWriteModeEnable{ true };

    bool bReadModeEnable{ true };

    bool bIsWorked{ false };

    bool bReadNextResponse{ false };

    bool bSaveOutputStates{ false };

    //*******************************************************************************************************************************************



    // Private variables
private:

    QList<Operation> OperationBuffer;

    QMutex OperationBufferMtx;

    std::string m_str_DeviceAddress;

    unsigned char m_cstr_DeviceAddress = 16;

    bool bReadyWrite{ true };

    bool bTriggeregState{ false };

    bool bIsConnected{ false };

};

inline void ModuleInfo::CalculateDeviceAddress()
{
    DeviceAddress_str = IO_Device::IntToHexStdString(DeviceAddress);
}

