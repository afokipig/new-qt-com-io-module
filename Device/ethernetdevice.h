#pragma once

#include "io_device.h"
#include <QTcpSocket>
#include "modbusbasic.h"

class EthernetDevice : public IO_Device, public ModbusBasic
{

    // Q_OBJECT Macro

    Q_OBJECT

    //*******************************************************************************************************************************************



    // Constructors and destructor
public:

    explicit EthernetDevice(QObject *parent = nullptr);

    //*******************************************************************************************************************************************



    // Public methods
public:

    virtual void SetModBusCheckFuntction(const ModBusCheckFunctions functionNum) override;

    virtual void SetModBusSetFunction(const ModBusSetFunctions functionNum) override;

    void Connect(const QString& ip, int port);

    //*******************************************************************************************************************************************



    // Protected methods
protected:

    virtual bool Reconnect(bool bForced) override;

    virtual void CheckDI() override;

    virtual void HandleModuleResponse(const QByteArray &response) override;

    virtual void SetRelayState(const unsigned long state) override;

    //*******************************************************************************************************************************************



    // Private methods
private:

    QByteArray arr;

    //*******************************************************************************************************************************************



    // Signals
signals:

    //*******************************************************************************************************************************************



    // Public slots
public slots:

    //*******************************************************************************************************************************************



    // Protected slots
protected slots:

    virtual void SpecialRead() override;

    virtual bool ConnectPrivate();

    //*******************************************************************************************************************************************



    // Private slots
private slots:

    //*******************************************************************************************************************************************



    // Public variables
public:

    //*******************************************************************************************************************************************



    // Protected variables
protected:

    QString ModuleIP;

    int ModulePort;

    QTcpSocket* Socket = nullptr;

    QTimer* SocketAlertTimer = nullptr;

    //*******************************************************************************************************************************************



    // Private variables
private:


};
