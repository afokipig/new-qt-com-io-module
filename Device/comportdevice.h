#pragma once

#include "io_device.h"

#include <QSerialPort>

class ComPortDevice : public IO_Device
{

    // Q_OBJECT Macro

    Q_OBJECT

    //*******************************************************************************************************************************************



    // Constructors and destructor
public:

    explicit ComPortDevice(QObject *parent = nullptr);

    //*******************************************************************************************************************************************



    // Public methods
public:

    //*******************************************************************************************************************************************



    // Protected methods
protected:

    static const QString FindDevice(const qint16 VID, const qint16 PID);

    const bool ConnectCOM();

    void HandlePortError(const QSerialPort::SerialPortError &error);

    virtual bool Reconnect(bool bForced = false) override;

    //*******************************************************************************************************************************************



    // Private methods
private:

    //*******************************************************************************************************************************************



    // Signals
signals:

    //*******************************************************************************************************************************************



    // Public slots
public slots:

    const bool InitPortAdapter(const int VID, const int PID, const int baundRate);

    //*******************************************************************************************************************************************



    // Protected slots
protected slots:

    //*******************************************************************************************************************************************



    // Private slots
private slots:

    const bool InitPortAdapterPrivate();

    void GetPortError(const QSerialPort::SerialPortError error);

    //*******************************************************************************************************************************************



    // Public variables
public:

    //*******************************************************************************************************************************************



    // Protected variables
protected:

    QSerialPort* SimpleUseDevice = nullptr;

    QList<QSerialPort::SerialPortError> ErrorList;

    QString PortNum;

    std::pair<int, int> Identifires;

    int PortBaudRate;

    //*******************************************************************************************************************************************



    // Private variables
private:




};

