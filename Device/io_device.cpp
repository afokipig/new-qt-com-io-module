#include "io_device.h"

#include <QDebug>

#include <sstream>

IO_Device::IO_Device(QObject *parent)
    : QObject(parent),
      AlertTimer(this),
      OperationBufferMtx(QMutex::NonRecursive)
{
    AlertTimer.setInterval(100);

    AlertTimer.setSingleShot(true);

    connect(&AlertTimer, &QTimer::timeout, this, &IO_Device::HandleTimeout);
}
//*******************************************************************************************************************************************



IO_Device::~IO_Device()
{
    Device->deleteLater();
}
//*******************************************************************************************************************************************



ModuleInfo &IO_Device::GetDeviceInfo()
{
    return DeviceInfo;
}
//*******************************************************************************************************************************************



void IO_Device::SetDeviceInfo(ModuleInfo &newDeviceInfo)
{
    DeviceInfo = newDeviceInfo;

    DeviceInfo.CalculateDeviceAddress();
}
//*******************************************************************************************************************************************



bool IO_Device::IsStarted() const
{
    return bIsStarted;
}
//*******************************************************************************************************************************************



void IO_Device::SetIsStarted(bool newBIsStarted)
{
    bIsStarted = newBIsStarted;
}
//*******************************************************************************************************************************************



bool IO_Device::b_WriteModeEnable() const
{
    return bWriteModeEnable;
}
//*******************************************************************************************************************************************



void IO_Device::Set_b_WriteModeEnable(bool newBWriteModeEnable)
{
    bWriteModeEnable = newBWriteModeEnable;
}
//*******************************************************************************************************************************************



bool IO_Device::b_ReadModeEnable() const
{
    return bReadModeEnable;
}
//*******************************************************************************************************************************************



void IO_Device::Set_b_ReadModeEnable(bool newBReadModeEnable)
{
    bReadModeEnable = newBReadModeEnable;
}
//*******************************************************************************************************************************************



bool IO_Device::IsWorked() const
{
    return bIsWorked;
}
//*******************************************************************************************************************************************



void IO_Device::SetIsWorked(bool newIsWorked)
{
    bIsWorked = newIsWorked;
}
//*******************************************************************************************************************************************



bool IO_Device::b_ReadNextResponse() const
{
    return bReadNextResponse;
}
//*******************************************************************************************************************************************



void IO_Device::Set_b_ReadNextResponse(bool newBReadNextResponse)
{
    bReadNextResponse = newBReadNextResponse;
}
//*******************************************************************************************************************************************



bool IO_Device::b_SaveOutputStates() const
{
    return bSaveOutputStates;
}
//*******************************************************************************************************************************************



void IO_Device::Set_b_SaveOutputStates(bool newBSaveOutputStates)
{
    bSaveOutputStates = newBSaveOutputStates;
}
//*******************************************************************************************************************************************



const std::string &IO_Device::str_DeviceAddress() const
{
    return m_str_DeviceAddress;
}
//*******************************************************************************************************************************************



char IO_Device::cstr_DeviceAddress() const
{
    return m_cstr_DeviceAddress;
}
//*******************************************************************************************************************************************



bool IO_Device::ReadyWrite() const
{
    return bReadyWrite;
}
//*******************************************************************************************************************************************



bool IO_Device::TriggeregState() const
{
    return bTriggeregState;
}
//*******************************************************************************************************************************************



void IO_Device::setTriggeregState(bool newTriggeregState)
{
    bTriggeregState = newTriggeregState;
}
//*******************************************************************************************************************************************



bool IO_Device::IsConnected() const
{
    return bIsConnected;
}
//*******************************************************************************************************************************************



void IO_Device::setIsConnected(bool newIsConnected)
{
    bIsConnected = newIsConnected;
}
//*******************************************************************************************************************************************



void IO_Device::Write(const QByteArray &message)
{
    try
    {

        if (Device->isOpen() && Device->isWritable() && b_ReadyWrite() && bIsWorked)
        {
            qDebug() << this << "Write " << QByteArray(message);

            Set_b_ReadyWrite(false);

            Device->write(message);

            if(!Device->waitForBytesWritten(-1))
            {

                Set_b_ReadyWrite(true);

                CheckReadiness();

            }

            AlertTimer.start();

        }

    } catch (std::exception &e)
    {

        qWarning() << "Port Exception" << e.what();

        emit SendInfoMessage(e.what());

        SetReadyWrite();

    } catch(...)
    {

        qWarning() << "Unexpected exception";

        emit SendInfoMessage("Unexpected exception");

        SetReadyWrite();

    }
}
//*******************************************************************************************************************************************



void IO_Device::Write(const char *message, const qint64 lenght)
{
    try
    {
        if (Device->isOpen() && Device->isWritable() && b_ReadyWrite() && bIsWorked)
        {

//            std::cout << "command " << lenght << '\n';
//            for (qint64 i = 0; i < lenght; ++i)
//            {
//                std::cout << std::hex << (int)message[i] << ' ';
//            }
//            std::cout << "\ncommand\n";

            Set_b_ReadyWrite(false);

            Device->write(message, lenght);

            if(!Device->waitForBytesWritten(-1))
            {

                Set_b_ReadyWrite(true);

                CheckReadiness();

            }

            AlertTimer.start();

        }

    } catch (std::exception &e)
    {

        qWarning() << "Port Exception" << e.what();

        emit SendInfoMessage(e.what());

        SetReadyWrite();

    } catch(...)
    {

        qWarning() << "Unexpected exception";

        emit SendInfoMessage("Unexpected exception");

        SetReadyWrite();

    }
}
//*******************************************************************************************************************************************



void IO_Device::ChangeRelayOutput(const int outputNumber, bool bIsActive)
{

    NewOutputState = OutputState;

    if(bIsActive)
    {
        NewOutputState |= (1 << (outputNumber));
    }
    else
    {
        NewOutputState &= ~(1 << (outputNumber));
    }

    if(NewOutputState != OutputState)
    {

        SetRelayState(NewOutputState);

        OutputState = NewOutputState;

    }
    else
    {

        CheckDI();

    }

}
//*******************************************************************************************************************************************



const std::string IO_Device::IntToHexStdString(const int &number)
{

    std::stringstream HEXStream;

    HEXStream << std::hex << number;

    std::string HEXString(HEXStream.str());

    for(auto & symbol : HEXString)
    {
        symbol = toupper(symbol);
    }

    if(HEXString.size() < 2)
    {
        HEXString.resize(2);

        HEXString.at(1) = HEXString.at(0);

        HEXString.at(0) = '0';
    }

    return HEXString;

}
//*******************************************************************************************************************************************



bool IO_Device::b_ReadyWrite() const
{
    return bReadyWrite;
}
//*******************************************************************************************************************************************



void IO_Device::Set_b_ReadyWrite(bool newBReadyWrite)
{
    bReadyWrite = newBReadyWrite;
}
//*******************************************************************************************************************************************



void IO_Device::CheckReadiness()
{

    //    qDebug() << "check" << !bIsStarted << bReadyWrite << !OperationBuffer.empty() << bWriteModeEnable << (bReadyWrite && !OperationBuffer.empty() && bWriteModeEnable) << OperationBuffer.size();

    if(!bIsStarted) return;

    if(bReadyWrite && !OperationBuffer.empty() && bWriteModeEnable)
    {

        switch (OperationBuffer.first().OperationType)
        {
        case TypeOfOperaton::EnableOut:

            ChangeRelayOutput(OperationBuffer.first().FirstArg, true);

            OperationBuffer.removeFirst();

            break;

        case TypeOfOperaton::DisableOut:

            ChangeRelayOutput(OperationBuffer.first().FirstArg, false);

            OperationBuffer.removeFirst();

            break;
        }

    }
    else if(bReadyWrite && bReadModeEnable)
    {

        CheckDI();

    }

}
//*******************************************************************************************************************************************



char IO_Device::OffBit(char byte, const int number)
{
    byte &= ~(1 << (number - 1));

    return byte;
}
//*******************************************************************************************************************************************



char IO_Device::OnBit(char byte, const int number)
{
    byte |= (1 << (number - 1));

    return byte;
}
//*******************************************************************************************************************************************



void IO_Device::SetIsConnected(bool newIsConnected)
{
    if(newIsConnected == bIsConnected) return;

    bIsConnected = newIsConnected;

    emit ConnectingStatusChanged(bIsConnected);
}
//*******************************************************************************************************************************************



void IO_Device::ReconnectToReadSpecialResponse()
{
    if(!Device)
    {
        emit SendInfoMessage("Device is null");

        qDebug() << this << "Device is null! Device" << Device;

        return;
    }

    connect(Device, &QIODevice::readyRead, this, &IO_Device::SpecialRead, Qt::QueuedConnection);

    disconnect(Device, &QIODevice::readyRead, this, &IO_Device::Read);

}
//*******************************************************************************************************************************************



void IO_Device::ConnectIODevice()
{
    if(!Device)
    {
        emit SendInfoMessage("Device is null");

        qDebug() << this << "Device is null! Device" << Device;

        return;
    }

    connect(Device, &QIODevice::readyRead, this, &IO_Device::Read, Qt::QueuedConnection);

    disconnect(Device, &QIODevice::readyRead, this, &IO_Device::SpecialRead);

}
//*******************************************************************************************************************************************



void IO_Device::AddOperation(const Operation& operation)
{
    OperationBufferMtx.lock();

    OperationBuffer.push_back(operation);

    OperationBufferMtx.unlock();
}
//*******************************************************************************************************************************************



const IO_Device::Operation &IO_Device::GetOperation()
{
    OperationBufferMtx.lock();

    const Operation& toReturn{ std::move(OperationBuffer.first()) };

    OperationBuffer.pop_front();

    OperationBufferMtx.unlock();

    return toReturn;
}
//*******************************************************************************************************************************************



void IO_Device::Start()
{

    if(QThread::currentThread() != thread())
    {
        QMetaObject::invokeMethod(this, "Start", Qt::QueuedConnection);

        return;
    }

    bIsStarted = true;

    bIsWorked = true;

    bReadyWrite = true;

    qDebug() << "Start";

    CheckReadiness();

}
//*******************************************************************************************************************************************



void IO_Device::Stop()
{

    if(QThread::currentThread() != thread())
    {
        QMetaObject::invokeMethod(this, "Stop", Qt::QueuedConnection);

        return;
    }
    bIsStarted = false;

    bIsWorked = false;

    qDebug() << "Stop";

}
//*******************************************************************************************************************************************



void IO_Device::ActivateDO(const int outputNumber)
{
    AddOperation(Operation(TypeOfOperaton::EnableOut, outputNumber));
}
//*******************************************************************************************************************************************



void IO_Device::DeactivateDO(const int outputNumber)
{
    AddOperation(Operation(TypeOfOperaton::DisableOut, outputNumber));
}
//*******************************************************************************************************************************************



void IO_Device::ToggleDO(const int outputNumber, const int shedulleTime, const int delayTime)
{
    if(QThread::currentThread() != thread())
    {
        QMetaObject::invokeMethod(this, "ToggleDO", Qt::QueuedConnection
                                  , Q_ARG(int, outputNumber)
                                  , Q_ARG(int, shedulleTime)
                                  , Q_ARG(int, delayTime));

        return;
    }

    QTimer *shedulleTimer = new QTimer(this);

    QTimer *delayTimer = new QTimer(this);

    shedulleTimer->setSingleShot(true);

    shedulleTimer->setInterval(shedulleTime);

    delayTimer->setSingleShot(true);

    delayTimer->setInterval(delayTime);

    connect(shedulleTimer, &QTimer::timeout, [&, shedulleTimer, delayTimer,this, outputNumber]()
    {

        ActivateDO(outputNumber);

        QMetaObject::invokeMethod(shedulleTimer, &QTimer::deleteLater);

        QMetaObject::invokeMethod(delayTimer, "start");

    });

    connect(delayTimer, &QTimer::timeout, [&, delayTimer, this, outputNumber]()
    {

        DeactivateDO(outputNumber);

        QMetaObject::invokeMethod(delayTimer, &QTimer::deleteLater);

    });

    QMetaObject::invokeMethod(shedulleTimer, "start");

}
//*******************************************************************************************************************************************



void IO_Device::SetReadyWrite()
{
    bReadyWrite = true;

    CheckReadiness();
}
//*******************************************************************************************************************************************



void IO_Device::HandleTimeout()
{

    if(bReadyWrite)
    {

        return;

    }
    else
    {

        if(badRequest++ > 5)
        {

            qDebug() << "Port not responsed Start Reconnect" << badRequest;

            emit SendInfoMessage("Port not responsed Start Reconnect " + QString::number(badRequest));

            Reconnect(true);

        }

        qDebug() << "Port not responsed" << badRequest;

        emit SendInfoMessage("Port not responsed " + QString::number(badRequest));

        SetReadyWrite();

    }

}
//*******************************************************************************************************************************************



void IO_Device::Read()
{

    AlertTimer.stop();

    if(Device && Device->isReadable())
    {
        HandleModuleResponse(Device->readAll());
    }

    SetReadyWrite();

}
//*******************************************************************************************************************************************



