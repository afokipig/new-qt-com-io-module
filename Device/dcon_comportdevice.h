#pragma once

#include "comportdevice.h"

class DCON_ComPortDevice : public ComPortDevice
{

    // Q_OBJECT Macro

    Q_OBJECT

    //*******************************************************************************************************************************************



    // Constructors and destructor
public:

    explicit DCON_ComPortDevice(QObject *parent = nullptr);

    //*******************************************************************************************************************************************



    // Public methods
public:

    bool NeedCRC() const;

    void set_NeedCRC(bool newBNeedCRC);

    void set_2byteState(bool newB2byteState);

    //*******************************************************************************************************************************************



    // Protected methods
protected:

    virtual void SetRelayState(unsigned long state) override;

    virtual void CheckDI() override;

    virtual void HandleModuleResponse(const QByteArray &response) override;

    //*******************************************************************************************************************************************



    // Private methods
private:

    const int CalculateCheckSum(char* command, int lenght);

    //*******************************************************************************************************************************************



    // Signals
signals:

    //*******************************************************************************************************************************************



    // Public slots
public slots:

    //*******************************************************************************************************************************************



    // Protected slots
protected slots:

    virtual void SpecialRead() override;

    //*******************************************************************************************************************************************



    // Private slots
private slots:

    //*******************************************************************************************************************************************



    // Public variables
public:

    //*******************************************************************************************************************************************



    // Protected variables
protected:

    //*******************************************************************************************************************************************



    // Private variables
private:

    bool bNeedCRC{ false };

    bool b2byteState{ false };

};
