#pragma once

#include <functional>

enum class ModBusCheckFunctions
{
    ModBus_1,
    ModBus_2,
    ModBus_3,
    ModBus_4
};

enum class ModBusSetFunctions
{
    ModBus_5,
    ModBus_6,
    ModBus_15,
    ModBus_16
};


class ModbusBasic
{
protected:

    explicit ModbusBasic(std::function<int(const int, const int, const int, char*, const int)> check, std::function<int(const int, const int, const int, char, char*, const int, char)> set);

    virtual void SetModBusCheckFuntction(const ModBusCheckFunctions functionNum) = 0;

    virtual void SetModBusSetFunction(const ModBusSetFunctions functionNum) = 0;

    std::function<int(const int, const int, const int, char*, const int)> CheckDigitalInput = nullptr;

    std::function<int(const int, const int, const int, char, char*, const int, char)> SetDigitalOutput = nullptr;
};

