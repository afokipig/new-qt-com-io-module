#include "modbusbasic.h"

ModbusBasic::ModbusBasic(std::function<int (const int, const int, const int, char *, const int)> check, std::function<int (const int, const int, const int, char, char *, const int, char)> set)
    : CheckDigitalInput{ check }
    , SetDigitalOutput{ set }
{

}
