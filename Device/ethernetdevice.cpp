#include "ethernetdevice.h"

#include "modbuswriter.h"

namespace ModBusTCP_Commands
{

int ModBusTCP_1_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(1, 0, out_command) };

    comLenght = ModBusWriter::ModBus_1_Command(++comLenght, deviceAddress, inputRegister, registersCount, out_command, lenght);

    return comLenght;
}
//*******************************************************************************************************************************************



int ModBusTCP_2_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(2, 0, out_command) };

    comLenght = ModBusWriter::ModBus_2_Command(++comLenght, deviceAddress, inputRegister, registersCount, out_command, lenght);

    return comLenght;
}
//*******************************************************************************************************************************************



int ModBusTCP_3_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(3, 0, out_command) };

    comLenght = ModBusWriter::ModBus_3_Command(++comLenght, deviceAddress, inputRegister, registersCount, out_command, lenght);

    return comLenght;
}
//*******************************************************************************************************************************************



int ModBusTCP_4_Command(const int deviceAddress, const int inputRegister, const int registersCount, char *out_command, const int lenght)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(4, 0, out_command) };

    comLenght = ModBusWriter::ModBus_4_Command(++comLenght, deviceAddress, inputRegister, registersCount, out_command, lenght);

    return comLenght;

}
//*******************************************************************************************************************************************



int ModBusTCP_5_Command(const int deviceAddress, const int outputRegister, const int outValue, char, char *out_command, const int lenght, char)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(5, 0, out_command) };

    comLenght = ModBusWriter::ModBus_5_Command(++comLenght, deviceAddress, outputRegister, outValue, out_command, lenght);

    return comLenght;

}
//*******************************************************************************************************************************************



int ModBusTCP_6_Command(const int deviceAddress, const int outputRegister, const int outValue, char, char *out_command, const int lenght, char)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(6, 0, out_command) };

    comLenght = ModBusWriter::ModBus_6_Command(++comLenght, deviceAddress, outputRegister, outValue, out_command, lenght);

    return comLenght;

}
//*******************************************************************************************************************************************



int ModBusTCP_15_Command(const int deviceAddress, const int outputRegister, const int registersCount, char byteMaskLO, char *out_command, const int lenght, char byteMaskHi)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(7, 0, out_command) };

    comLenght = ModBusWriter::ModBus_15_Command(++comLenght, deviceAddress, outputRegister, registersCount, byteMaskLO, out_command, lenght, byteMaskHi);

    return comLenght;

}
//*******************************************************************************************************************************************



int ModBusTCP_16_Command(const int deviceAddress, const int outputRegister, const int registersCount, char byteMaskLO, char *out_command, const int lenght, char byteMaskHi)
{

    int comLenght{ ModBusWriter::ModBusTCP_Header(8, 0, out_command) };

    comLenght = ModBusWriter::ModBus_16_Command(++comLenght, deviceAddress, outputRegister, registersCount, byteMaskLO, out_command, lenght, byteMaskHi);

    return comLenght;

}
//*******************************************************************************************************************************************
}




EthernetDevice::EthernetDevice(QObject *parent)
    : IO_Device(parent)
    , ModbusBasic{ ModBusTCP_Commands::ModBusTCP_3_Command,
                   ModBusTCP_Commands::ModBusTCP_16_Command }
{}
//*******************************************************************************************************************************************



void EthernetDevice::SetModBusCheckFuntction(const ModBusCheckFunctions functionNum)
{

    switch (functionNum)
    {
    case ModBusCheckFunctions::ModBus_1:

        CheckDigitalInput = ModBusTCP_Commands::ModBusTCP_1_Command;

        break;

    case ModBusCheckFunctions::ModBus_2:

        CheckDigitalInput = ModBusTCP_Commands::ModBusTCP_2_Command;

        break;

    case ModBusCheckFunctions::ModBus_3:

        CheckDigitalInput = ModBusTCP_Commands::ModBusTCP_3_Command;

        break;

    case ModBusCheckFunctions::ModBus_4:

        CheckDigitalInput = ModBusTCP_Commands::ModBusTCP_4_Command;

        break;

    }

}
//*******************************************************************************************************************************************



void EthernetDevice::SetModBusSetFunction(const ModBusSetFunctions functionNum)
{

    switch (functionNum)
    {
    case ModBusSetFunctions::ModBus_5:

        SetDigitalOutput = ModBusTCP_Commands::ModBusTCP_5_Command;

        break;

    case ModBusSetFunctions::ModBus_6:

        SetDigitalOutput = ModBusTCP_Commands::ModBusTCP_6_Command;

        break;

    case ModBusSetFunctions::ModBus_15:

        SetDigitalOutput = ModBusTCP_Commands::ModBusTCP_15_Command;

        break;

    case ModBusSetFunctions::ModBus_16:

        SetDigitalOutput = ModBusTCP_Commands::ModBusTCP_16_Command;

        break;

    }

}
//*******************************************************************************************************************************************



void EthernetDevice::Connect(const QString &ip, int port)
{
    ModuleIP = ip;

    ModulePort = port;

    QMetaObject::invokeMethod(this, &EthernetDevice::ConnectPrivate, Qt::QueuedConnection);
}
//*******************************************************************************************************************************************



bool EthernetDevice::Reconnect(bool bForced)
{

    if(Socket->state() != QTcpSocket::UnconnectedState && !bForced)
    {

        return true;

    }

    SetIsConnected(false);

    if(Socket)
    {

        Socket->disconnectFromHost();

    }

    AlertTimer.stop();

    bIsWorked = false;

    qInfo() << this << "Start reconnect";

    return ConnectPrivate();

}
//*******************************************************************************************************************************************



bool EthernetDevice::ConnectPrivate()
{

    if(!Socket)
    {

        Socket = new QTcpSocket();

        Device = Socket;

        Socket->moveToThread(thread());

//        Socket->setParent(this);

        connect(Socket, &QTcpSocket::readyRead, this, &EthernetDevice::Read, Qt::QueuedConnection);

//        connect(Socket, &QTcpSocket::stateChanged, this, &EthernetDevice::GetSocketState, Qt::QueuedConnection);

        SocketAlertTimer = new QTimer();

        SocketAlertTimer->setInterval(1000);

//        connect(SocketAlertTimer, &QTimer::timeout, this, &EthernetDevice::CheckSocket, Qt::QueuedConnection);

        AlertTimer.setInterval(600);

        AlertTimer.setSingleShot(true);

        connect(&AlertTimer, &QTimer::timeout, this, &EthernetDevice::HandleTimeout, Qt::QueuedConnection);

    }

    Socket->connectToHost(ModuleIP, ModulePort, QTcpSocket::ReadWrite);

    bool bIsSuccess = Socket->waitForConnected(100);

    qInfo() << "Connect " + ModuleIP << bIsSuccess;

    if(!SocketAlertTimer->isActive())
    {

        SocketAlertTimer->start();

    }

    bIsWorked = bIsSuccess;

    emit SendSensorInitResult(ModuleIP, bIsSuccess);

    SetIsConnected(bIsSuccess);

    return  bIsSuccess;

}/*******************************************************************************************************************************************/



void EthernetDevice::CheckDI()
{

    if(!CheckDigitalInput)
    {

        qDebug() << this << "Check DI function is NULL";

        return;
    }

    NextCommandSize = CheckDigitalInput(DeviceInfo.DeviceAddress, DeviceInfo.InputRegister, DeviceInfo.InputRegisterCount, NextCommand, MAX_COMMAND_SIZE);

    bReadNextResponse = true;

    NextCommand[5] = NextCommandSize - 5;

    Write(NextCommand, NextCommandSize + 1);

}
//*******************************************************************************************************************************************



void EthernetDevice::HandleModuleResponse(const QByteArray &response)
{

    if(bReadNextResponse)
    {
        emit SensorState((long)response.at(10));
    }

}
//*******************************************************************************************************************************************



void EthernetDevice::SetRelayState(const unsigned long state)
{

    if(!SetDigitalOutput)
    {

        qDebug() << this << "Set function is NULL";

        return;
    }

    NextCommandSize = SetDigitalOutput(DeviceInfo.DeviceAddress, DeviceInfo.OutputRegister, DeviceInfo.OutputRegisterCount, state, NextCommand, MAX_COMMAND_SIZE, 0);

    NextCommand[5] = NextCommandSize - 5;

    bReadNextResponse = false;

    qDebug() << QByteArray(NextCommand, NextCommandSize + 1);

    Write(NextCommand, NextCommandSize + 1);

}
//*******************************************************************************************************************************************



void EthernetDevice::SpecialRead()
{

}
//*******************************************************************************************************************************************



