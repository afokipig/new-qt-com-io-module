QT -= gui

QT += network serialport

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        Device/comportdevice.cpp \
        Device/dcon_comportdevice.cpp \
        Device/ethernetdevice.cpp \
        Device/io_device.cpp \
        Device/modbusbasic.cpp \
        Device/modbuscomportdevice.cpp \
        Device/modbuswriter.cpp \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    Device/Includes.h \
    Device/comportdevice.h \
    Device/dcon_comportdevice.h \
    Device/ethernetdevice.h \
    Device/io_device.h \
    Device/modbusbasic.h \
    Device/modbuscomportdevice.h \
    Device/modbuswriter.h \
    IO_Devices_global.h \
    io_devices.h
